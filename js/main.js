new Vue({
  el: '#app',
  data: {

    //todo variables
    submittable: false,
    newTodoText: '',
    newTodoTitle: '',
    newTodoColour: [],

    //selected filters
    showColours: [],

    //list of todos
    todos: [
      {
        text: 'Welcome',
        title: 'Create new Todos above',
        colour: 'blue',
      }
    ],

    //list of available filters
    colours: [
      "blue",
      "red",
      "yellow",
      "green",
    ],
  },
  methods: {

    //save todo to array
    addTodo: function () {
      var text = this.newTodoText.trim();
      var title = this.newTodoTitle.trim();
      var colour = this.newTodoColour;

      if (text && title && colour) {
        this.todos.push({
          text: text,
          title: title,
          colour: colour
        });

        this.newTodoText = '';
        this.newTodoTitle = '';
        this.newTodoColours = [];
      }
    },

    /**
    * Remove todo from list
    * @param {int} inex of todo to remove
    */
    removeTodo: function (index) {
      this.todos.splice(index, 1)
    }
  }
});
